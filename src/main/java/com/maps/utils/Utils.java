package com.maps.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebElement;
import org.testng.annotations.Test;

import com.maps.baseclass.BaseClass;
/**
 * 
 * This is utils class, Which has got few functions which are commonly used across the project.
 * For example, For taking screen shots of the error, Dropdown selection on any page.
 *
 */
public class Utils {

	public void takeScreenshot(String folderName, String screenshotName) {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");
		File file = new File(df.format(date));
		try {
			TakesScreenshot ts = (TakesScreenshot) BaseClass.driver;
			File source = (File) ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source,
					new File("./Screenshots/" + folderName + "/" + file + "_" + screenshotName + ".png"));
			System.out.println("Screenshot taken");
		} catch (Exception e) {
			System.out.println("Error message is... " + e.getMessage());
		}
	}

	public String APICall(String lon1, String lat1, String lon2, String lat2) throws Exception
	{
		//main url is 
		String url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+lon1+",-"+lat1+"&destinations="+lon2+",-"+lat2+"&key=AIzaSyDOVwsX8WJ05kLO54wvw6gVnr4nRrVLBLo";

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		// optional default is GET
		con.setRequestMethod("GET");
		//add request header
		// con.setRequestProperty("User-Agent", "Chrome/5.0");
		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		//print in String
		System.out.println(response.toString());
		//Read JSON response and print
		JSONObject myResponse = new JSONObject(response.toString());
		String distance =myResponse.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("distance").getString("text");
		System.out.println(distance);
		return distance;  

	}
}
