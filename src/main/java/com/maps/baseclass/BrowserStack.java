package com.maps.baseclass;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidDriver;

public class BrowserStack {

	/*Upload your Android app (.apk file) or iOS app (.ipa file) to the BrowserStack servers using the REST API.
	curl -u "USERNAME:ACCESS_KEY" \
	-X POST "https://api-cloud.browserstack.com/app-automate/upload" \
	-F "file=@/path/to/app/file/Application-debug.apk"
	{"app_url":"bs://<hashed appid>"}
	*/
	
	
	public static String userName = "erw1";
	public static String accessKey = "fUCbFdLXn9TRMsUeWn9Q";

	public static void main(String args[]) throws MalformedURLException, InterruptedException {
		DesiredCapabilities caps = new DesiredCapabilities();

		caps.setCapability("device", "Google Pixel");
		caps.setCapability("app", "bs://1133e069986314b83752c9063ec2761bf78cfe21");
		caps.setCapability("os_version", "7.1");
		caps.setCapability("Platform", "ANY");
		caps.setCapability("browserstack.debug", "true");



		AndroidDriver driver = new AndroidDriver(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), caps);

		System.out.println("app launched");

		WebElement searchbutton= new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//android.widget.ImageView[@content-desc='Search Wikipedia'])[1]")));
		searchbutton.click();
		System.out.println("clicked");
		WebElement searchElement = new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.id("org.wikipedia.alpha:id/search_src_text")));	
		searchElement.sendKeys("test");
		System.out.println("text entered");

//		
//				WebElement searchElement = new WebDriverWait(driver, 30).until(
//						ExpectedConditions.elementToBeClickable(By.id("Search Wikipedia")));
//				searchElement.click();
//				WebElement insertTextElement = new WebDriverWait(driver, 30).until(
//						ExpectedConditions.elementToBeClickable(By.id("org.wikipedia.alpha:id/search_src_text")));
//				insertTextElement.sendKeys("BrowserStack");
//				Thread.sleep(5000);
//		
//				List allProductsName = driver.findElements(By.className("android.widget.TextView"));
//				assert(allProductsName.size() > 0);

		driver.quit();
	}
}
