package com.maps.baseclass;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;
import java.net.URL;
import java.net.MalformedURLException;

public class BaseClass {

	 //public static IOSDriver<IOSElement> driver;

	   public static AppiumDriver<MobileElement> driver;
	   public static String accessKey = "BxApmeoozxzxAx6iLtzE";
	   public static String userName = "vinita13";
		  
	   @BeforeClass(alwaysRun = true)
	   public static void LaunchApp() throws MalformedURLException, InterruptedException {

		DesiredCapabilities capabilities = new DesiredCapabilities();
					
		//capabilities.setCapability("device", "Google Pixel");
       // capabilities.setCapability("os_version", "7.1");
      //  capabilities.setCapability("app", "bs://81529e5944614916f97064438f21efd511d98f47");
        
     //  driver = new AndroidDriver(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), capabilities);
        
		//************ For Android Device *****************//    	          	
		/*capabilities.setCapability("automationName", "uiautomator2");    
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Android device");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, " com.google.android.apps.maps");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.google.android.maps.MapsActivity");
    
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.1.1:4727/wd/hub"), capabilities);
      */
			
        //************ For BrowserStack Connection *****************//
        capabilities.setCapability("device", "Google Pixel");
        capabilities.setCapability("os_version", "7.1");
        capabilities.setCapability("app", "bs://981efa0165115da27f3ed69f4c665778211dbffe");
      //  capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, " com.google.android.apps.maps");
      //  capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.google.android.maps.MapsActivity");

        driver = new AndroidDriver<MobileElement>(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), capabilities);
        
        driver.findElement(By.xpath("//*[@text='SKIP']")).click();        
        
      //************ For iOS Device Connection *****************//
		/*capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability("platformVersion", "11.1.2");
		capabilities.setCapability("deviceName", "Mandar Vaidya’s iPhone");
		capabilities.setCapability("udid", "336e2939620b91a06cbfc5146e3d00b30f6cf855");
		capabilities.setCapability("automationName", "XCUITest");
		//	capabilities.setCapability("bundleId", "com.apple.Maps");
		capabilities.setCapability("bundleId", "com.google.Maps");
		capabilities.setCapability("showXcodeLog", "true");
		capabilities.setCapability(MobileCapabilityType.NO_RESET, "true");  
		driver = new IOSDriver<IOSElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);*/
		
		 
	}
	

}