package com.maps.inputparsing;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
/**
 * 
 * This class is used to parse the input file
 *
 */
public class ConfigReader {
	Properties pro;

	public ConfigReader() {

		try {

			File src = new File("properties/MobileConfig.property");
			FileInputStream fis = new FileInputStream(src);
			pro = new Properties();
			pro.load(fis);
			System.out.println(pro);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public String InputData() {
		return pro.getProperty("InputData");
	}

	public String OutputReport() {
		return pro.getProperty("OutputReport");
	}

}
