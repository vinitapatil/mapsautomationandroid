package com.maps.inputparsing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**
 * 
 * This class is used to parse the excel inout file.
 * Contains functions which are usefull in reading and writing the excel content.
 *
 */
public class ExcelDataConfig {

	XSSFWorkbook wb;
	XSSFSheet sheet1;
	int rowcount;
	File src;
	FileInputStream fis;
	FileOutputStream fout;

	int TestCaseRow;

	public ExcelDataConfig(String excelPath, String sheetName) {

		try {

			src = new File(excelPath);
			fis = new FileInputStream(src);
			wb = new XSSFWorkbook(fis);
			sheet1 = wb.getSheet(sheetName);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public String getData(int row, int column) {
		String data = sheet1.getRow(row).getCell(column).getStringCellValue();
		return data;
	}

	public int getRowcount() {
		int row = sheet1.getLastRowNum();
		return row;
	}

	public int getColumncount(int rowNo) {
		int column = sheet1.getRow(rowNo).getLastCellNum();
		return column;
	}

	public String Readvalue(int row, int column) {
		String celltext = sheet1.getRow(row).getCell(column).getStringCellValue();
		return celltext;
	}

	public String Readvalue(int rowIndex, String colname) {
		int colindex = 0;
		for (int i = 0; i < getColumncount(0); i++) {
			if (sheet1.getRow(0).getCell(i).getStringCellValue().equalsIgnoreCase(colname)) {
				colindex = i;
				break;
			}
		}
		return Readvalue(rowIndex, colindex);
	}

	public void WriteCell(int rowIndex, int column, String cellText) {
		sheet1.getRow(rowIndex).createCell(column).setCellValue(cellText);

		try {
			fout = new FileOutputStream(src);
			wb.write(fout);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void WriteCell(int rowIndex, String columnName, String cellText) {
		int columnIndex = 0;
		for (int i = 0; i < getColumncount(0); i++) {
			if (sheet1.getRow(0).getCell(i).getStringCellValue().equalsIgnoreCase(columnName)) {
				columnIndex = i;
				break;
			}
		}
		WriteCell(rowIndex, columnIndex, cellText);
	}

	public void WriteDate(int rowIndex, int column) {
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");

		sheet1.getRow(rowIndex).createCell(column).setCellValue(df.format(date));

		try {
			fout = new FileOutputStream(src);
			wb.write(fout);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void WriteDate(int rowIndex, String columnName) {
		int columnIndex = 0;
		for (int i = 0; i < getColumncount(0); i++) {
			if (sheet1.getRow(0).getCell(i).getStringCellValue().equalsIgnoreCase(columnName)) {
				columnIndex = i;
				break;
			}
		}
		WriteDate(rowIndex, columnIndex);
	}

}
