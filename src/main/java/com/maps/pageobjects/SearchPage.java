package com.maps.pageobjects;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import com.maps.beans.SearchBean;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

/**
 * 
 * This class is used to get the components of search page
 */
public class SearchPage {

	public AppiumDriver<MobileElement> driver;

	SearchBean login = new SearchBean();

	public SearchPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
	}

	public boolean sourcesearchField(String source) {
		System.out.println("Inside searchField method...");
		try {
			WebElement searchTextField = driver.findElement(login.getSourceSearchBar());
			System.out.println("searchTextField is present");
			searchTextField.clear();
			searchTextField.sendKeys(source);
			System.out.println("searchText " + source + " has been entered");
			return true;
		} catch (Exception e) {
			System.out.println("searchTextField was not found");
			return false;
		}
	}
	public boolean destinationsearchField(String destination) {
		System.out.println("Inside searchField method...");
		try {
			List <MobileElement> searchTextField = driver.findElements(login.getDestinationSearchBar());		
			searchTextField.clear();
			searchTextField.get(0).sendKeys(destination);
			System.out.println("searchTextField is present");
			System.out.println("searchText " + destination + " has been entered");
			return true;
		} catch (Exception e) {
			System.out.println("searchTextField was not found");
			WebElement searchTextField = driver.findElement(login.getDestinationSearchBar());		
			searchTextField.clear();
			searchTextField.sendKeys(destination);
			return false;
		}
	}
	public boolean sourceField(String source) {
		System.out.println("Inside sourceField method...");
		try {
			List <MobileElement> sourceTextField = driver.findElements(login.getSource());
			System.out.println("sourceTextField is present");
			System.out.println(sourceTextField.get(0).getText());
			System.out.println(sourceTextField.get(1).getText());
			sourceTextField.get(0).click();
			//sourceTextField.sendKeys(source);
			//System.out.println("sourceText " + source + " has been entered");
			return true;
		} catch (Exception e) {
			System.out.println("sourceTextField was not found");
			return false;
		}
	}
		
	public boolean destinationField(String destination) {
		System.out.println("Inside destinationField method...");
		try {
			List <MobileElement> destinationTextField = driver.findElements(login.getDestination());
			//WebElement searchTextField = driver.findElement(login.getDestination());
			System.out.println("searchTextField is present");
			System.out.println(destinationTextField.get(0).getText());
			System.out.println(destinationTextField.get(1).getText());
			destinationTextField.get(1).click();
			//searchTextField.sendKeys(destination);
			//System.out.println("destinationText " + destination + " has been entered");
			return true;
		} catch (Exception e) {
			System.out.println("destinationTextField was not found");
			List <MobileElement> destinationTextField = driver.findElements(login.getDestination());
			System.out.println(destinationTextField.get(0).getText());
			System.out.println(destinationTextField.get(1).getText());
			destinationTextField.get(1).click();
			return false;
		}
	}
	public boolean GoNavigationButton() {
		System.out.println("Inside GoButton method...");
		try {
			
			WebElement goButton = driver.findElement(login.getGoNavigation());
			goButton.click();
			System.out.println("GoNavigationButton has been clicked");
			return true;
		} catch (Exception e) {
			System.out.println("GoNavigationButton has not been clicked");
			return false;
		}
	}
	public boolean GoButton() {
		System.out.println("Inside GoButton method...");
		try {
			WebElement goButton = driver.findElement(login.getGo());
	//((AndroidDriver<MobileElement>)driver).pressKeyCode(66);
			//((AndroidDriver<MobileElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_SEARCH);
			goButton.click();
			
			System.out.println("Go button has been clicked");
			return true;
		} catch (Exception e) {
			//((AndroidDriver<MobileElement>) driver).pressKeyCode(AndroidKeyCode.KEYCODE_SEARCH);
			
			System.out.println("Go button has not been clicked");
			//((AndroidDriver<MobileElement>)driver).pressKeyCode(66);
			return false;
		}
	}
	public String DistanceOutput() throws InterruptedException {
		System.out.println("Inside DistanceOutput method...");
		WebElement ele = driver.findElement(By.xpath("//android.widget.TextView[@index='2']"));
		System.out.println("Element found");
		String text = ele.getText();
		System.out.println("text is ..."+text);
		Thread.sleep(3000);
		String str = text.replaceAll("[^\\d.]", "");
/*		String regex = "[0-9]*['.']?[0-9]*";
		Pattern pattern = Pattern.compile(regex);

		//Pattern pattern = Pattern.compile("((.*?)km)");
		Matcher matcher = pattern.matcher(str);*/
		/*while (matcher.find()) {
			System.out.println("Distance found on google map is "+matcher.group(1)+" kilometers");
			return matcher.group(1);
		}
		*/
		System.out.println("Distance found on google map is "+str+" kilometers");
		((AndroidDriver<MobileElement>)driver).pressKeyCode(4);
		return str;
		
	}

	public boolean selectFromDropdown(String name) {
		try {
			RemoteWebElement element = (RemoteWebElement) driver.findElement(By.id(name));
			String elementID = element.getId();
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("element", elementID);
			scrollObject.put("toVisible", "not an empty string");
			driver.executeScript("mobile:scroll", scrollObject);
			element.click();
			return true;
		} catch (Exception e) {
			System.out.println("Error message received is " + e.getMessage());
			return false;
		}
	}
	public boolean BackButton() {
		System.out.println("Inside GoButton method...");
		try {
			//WebElement backButton = driver.findElement(By.id("header_back_btn"));
			//backButton.click();
			((AndroidDriver<MobileElement>)driver).pressKeyCode(4);
			System.out.println("backButton has been clicked");
			return true;
		} catch (Exception e) {
			System.out.println("backButton has not been clicked");
			return false;
		}
	}


}
