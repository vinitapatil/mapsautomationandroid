/**
 * 
 * @author Ujjwal Abhishek
 * This class is used to automate the Login functionality of the LensApp 
 */
package com.maps.testscripts;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.maps.baseclass.BaseClass;
import com.maps.inputparsing.ConfigReader;
import com.maps.inputparsing.ExcelDataConfig;
import com.maps.pageobjects.SearchPage;
import com.maps.utils.Utils;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class SearchTest extends BaseClass {

	ExtentTest logger;

	SearchPage search;
	ConfigReader config = new ConfigReader();
	ExcelDataConfig LoginDataSheet = new ExcelDataConfig(config.InputData(), "Data");
	ExcelDataConfig OutputSheet = new ExcelDataConfig(config.OutputReport(), "Data");
	//	ExcelDataConfig OutputSheet = new ExcelDataConfig(config.Output(), "LoginData");
	Utils utils = new Utils();

	Date date = new Date();
	SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss");
	String simpledate = df.format(date);

	ExtentReports report = new ExtentReports("./extentreport/SearchReport_"+simpledate+".html", false);


	@Test(dataProvider = "Data", dataProviderClass = com.maps.dataprovider.Search.class)
	public void loginCheck(Map<String, String> cart) throws Exception {
		System.out.println("Inisde login test");

		String latitude1 = cart.get("Latitude1").toString();
		String longitude1 = cart.get("Longitude1").toString();
		String latitude2 = cart.get("Latitude2").toString();
		String longitude2 = cart.get("Longitude2").toString();
		String source  = cart.get("Source").toString();
		String destination  = cart.get("Destination").toString();
		String serialNo = cart.get("SerialNo").toString();
		Integer rowNo = Integer.parseInt(serialNo);
		System.out.println("row no. is "+rowNo);


		System.out.println("Executing the serial number " + serialNo);
		logger = report.startTest("SearchPageTest");
		search = new SearchPage(driver);
        
		

		if(search.GoNavigationButton())
		{
					
			logger.log(LogStatus.PASS, "GoNavigationButton clicked");
			System.out.println("Go Button clicked");
		}
		else
		{
			logger.log(LogStatus.FAIL, "GoNavigationButton not clicked");
			System.out.println("Go Button could not be clicked");
			utils.takeScreenshot("Search", "SearchTest");
		}
		Thread.sleep(2000);
		if(search.sourceField(source))
		{
			logger.log(LogStatus.PASS, "source "+source+" entered");
			System.out.println("source "+source+" entered");
		}
		else
		{
			logger.log(LogStatus.FAIL, "source "+source+" could not be entered");
			System.out.println("source "+source+" could not be entered");
			utils.takeScreenshot("Search", "SearchTest");
		}
		Thread.sleep(2000);
		
		if(search.sourcesearchField(source))
		{
			logger.log(LogStatus.PASS, "source "+source+" entered");
			System.out.println("source "+source+" entered");
		}
		else
		{
			logger.log(LogStatus.FAIL, "source "+source+" could not be entered");
			System.out.println("source "+source+" could not be entered");
			utils.takeScreenshot("Search", "SearchTest");
		}
		Thread.sleep(2000);
		if(search.GoButton())
		{
			logger.log(LogStatus.PASS, "Go button in keyboard has been clicked");
			System.out.println("Go button in keyboard has been clicked");
		}
		else
		{
			logger.log(LogStatus.FAIL, "Go button in keyboard could not  be clicked");
			System.out.println("Go button in keyboard could not  be clicked");
			utils.takeScreenshot("Search", "SearchTest");
		}
		if(search.destinationField(destination))
		{
			logger.log(LogStatus.PASS, "destination "+destination+" entered");
			System.out.println("destination "+destination+" entered");
		}
		else
		{
			logger.log(LogStatus.FAIL, "destination "+destination+" could not be entered");
			System.out.println("destination "+destination+" could not be entered");
			utils.takeScreenshot("Search", "SearchTest");
		}
		if(search.destinationsearchField(destination))
		{
			logger.log(LogStatus.PASS, "destination "+destination+" entered");
			System.out.println("destination "+destination+" entered");
		}
		else
		{
			logger.log(LogStatus.FAIL, "destination "+destination+" could not be entered");
			System.out.println("destination "+destination+" could not be entered");
			utils.takeScreenshot("Search", "SearchTest");
		}
		Thread.sleep(2000);
		if(search.GoButton())
		{
			logger.log(LogStatus.PASS, "Go button in keyboard has been clicked");
			System.out.println("Go button in keyboard has been clicked");
		}
		else
		{
			logger.log(LogStatus.FAIL, "Go button in keyboard could not  be clicked");
			System.out.println("Go button in keyboard could not  be clicked");
			utils.takeScreenshot("Search", "SearchTest");
		}
		
		
		Thread.sleep(2000);	
		String DistanceOnMap = search.DistanceOutput()+" km";
		logger.log(LogStatus.INFO, "Distance on map is "+DistanceOnMap);
		System.out.println("the distance on google maps is "+DistanceOnMap);
		String APIDistance = utils.APICall(latitude1,longitude1,latitude2,longitude2);
		logger.log(LogStatus.INFO, "API distance is "+APIDistance);
		System.out.println("api dist is "+APIDistance);

		if(DistanceOnMap.equalsIgnoreCase(APIDistance))
		{
			logger.log(LogStatus.PASS, "Both distances are same");
			System.out.println("Both distances are same");
			OutputSheet.WriteCell(rowNo, "MapDistance", DistanceOnMap);
			OutputSheet.WriteCell(rowNo, "APIDistance", APIDistance);
			OutputSheet.WriteCell(rowNo, "Status", "Pass");
			OutputSheet.WriteCell(rowNo, "Comment", "Both distances are same");
			OutputSheet.WriteDate(rowNo, "Date");
			//search.BackButton();
			Thread.sleep(3000);

		}
		else
		{
			logger.log(LogStatus.FAIL, "Both distance are not same..");
			System.out.println("Both distance are not same..");
			OutputSheet.WriteCell(rowNo, "MapDistance", DistanceOnMap);
			OutputSheet.WriteCell(rowNo, "APIDistance", APIDistance);
			OutputSheet.WriteCell(rowNo, "Status", "Fail");
			OutputSheet.WriteCell(rowNo, "Comment", "Both distances are not same");
			OutputSheet.WriteDate(rowNo, "Date");
			utils.takeScreenshot("Search", "SearchTest");
			search.BackButton();
			Thread.sleep(3000);

		}    
		
		

		report.endTest(logger);
		report.flush();
	}  
}
