package com.maps.beans;

import org.openqa.selenium.By;

public class SearchBeanAndroid {

		
	private By searchBar = By.name("Search here");
	private By source = By.id("Your location");
	private By destination = By.id("Choose destination…");
	private By go = By.id("Go");
	private By goNavigation = By.xpath("//android.widget.TextView[@text='Drive']");
	
	public By getGoNavigation() {
		return goNavigation;
	}
	public void setGoNavigation(By goNavigation) {
		this.goNavigation = goNavigation;
	}
	public By getSource() {
		return source;
	}
	public void setSource(By source) {
		this.source = source;
	}
	public By getDestination() {
		return destination;
	}
	public void setDestination(By destination) {
		this.destination = destination;
	}
	public By getGo() {
		return go;
	}
	public void setGo(By go) {
		this.go = go;
	}
	public By getSearchBar() {
		return searchBar;
	}
	public void setSearchBar(By searchBar) {
		this.searchBar = searchBar;
	}
}
