package com.maps.beans;

import org.openqa.selenium.By;

public class SearchBean {
	
	private By sourceSearchBar = By.xpath("//android.widget.EditText[@index='0']");
	private By destinationSearchBar = By.className("android.widget.EditText");
	private By source = By.className("android.widget.TextView");
	private By destination = By.className("android.widget.TextView");
	private By go = By.xpath("//android.widget.RelativeLayout[@index='0']");
	private By goNavigation = By.xpath("//android.widget.TextView[@text='GO']");
	  	    
	/*private By searchBar = By.name("Search for a place or address");
	private By source = By.id("Your location");
	private By destination = By.id("Choose destination…");
	private By go = By.id("Go");
	private By goNavigation = By.id("root_header_direction_search_btn");*/
			
	public By getGoNavigation() {
		return goNavigation;
	}
	public void setGoNavigation(By goNavigation) {
		this.goNavigation = goNavigation;
	}
	
	public By getSource() {
		return source;
	}
	public void setSource(By source) {
		this.source = source;
	}
	public By getDestination() {
		return destination;
	}
	public void setDestination(By destination) {
		this.destination = destination;
	}
	public By getGo() {
		return go;
	}
	public void setGo(By go) {
		this.go = go;
	}
	public By getSourceSearchBar() {
		return sourceSearchBar;
	}
	public void setSourceSearchBar(By searchBar) {
		this.sourceSearchBar = sourceSearchBar;
	}
	public By getDestinationSearchBar() {
		return destinationSearchBar;
	}
	public void setDestinationSearchBar(By searchBar) {
		this.destinationSearchBar = destinationSearchBar;
	}
}
